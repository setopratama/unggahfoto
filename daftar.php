<?php
    include 'header.php';
    include 'config/database.php';
?>
 <div class="container">
        <div class="row mt-5">
            <div class="col-6">
            <form>
        <div class="form-group">
          <label for="email">Email address</label>
          <input type="email" class="form-control" id="email" aria-describedby="emailHelp">
          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
          <label for="retype-email">Retype email address</label>
          <input type="email" class="form-control" id="retype-email">
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" class="form-control" id="password">
        </div>
        <button type="submit" class="btn btn-primary mt-4">Submit</button>
      </form>
            </div>
        </div>
    </div>
<?php 
    include 'footer.php';
?>