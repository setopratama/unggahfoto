  <!-- Footer -->
  <footer class="bg-light py-5 mt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <p>Copyright &copy; 2022 Website UGC</p>
        </div>
        <div class="col-md-6">
          <ul class="list-inline text-md-right">
            <li class="list-inline-item"><a href="#">Terms of Use</a></li>
            <li class="list-inline-item"><a href="#">Privacy Policy</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
  <!-- End Footer -->
  </body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.16.6/dist/umd/popper.min.js" integrity="sha384-KXlZvG8ZDlL0G/Oj7Vu1vqf/7VNjt0FklO2cNvzR0tE5w5G5Y5ZjKz1HJxZlEqP" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</html>