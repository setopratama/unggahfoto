<?php
    include 'header.php';
?>
<div class="container">
    <div class="row">
        <div class="col-6 mt-5">
    <form>
  <div class="form-group">
    <label for="name">Nama</label>
    <input type="text" class="form-control" id="name" placeholder="Masukkan nama Anda">
  </div>
  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" id="email" placeholder="Masukkan email Anda">
  </div>
  <div class="form-group">
    <label for="subject">Subjek</label>
    <input type="text" class="form-control" id="subject" placeholder="Masukkan subjek pesan Anda">
  </div>
  <div class="form-group">
    <label for="message">Pesan</label>
    <textarea class="form-control" id="message" rows="3" placeholder="Tuliskan pesan Anda disini"></textarea>
  </div>
  <button type="submit" class="btn btn-primary mt-2">Kirim</button>
</form>
</div>
    </div>
</div>
<?php 
    include 'footer.php';
?>