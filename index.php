<?php 
    include 'header.php';
    include 'slide.php';
?>
    <!-- Features -->
    <div class="container mt-5">
    <div class="row">
      <div class="col-md-4">
        <h3>Easy to use</h3>
        <p>Our website is designed to be easy and intuitive to use, so you can focus on sharing your photos with others.</p>
      </div>
      <div class="col-md-4">
        <h3>Social features</h3>
        <p>Interact with other users by leaving comments and giving likes on their photos. Connect with friends and family by sharing your photos with them.</p>
      </div>
      <div class="col-md-4">
        <h3>Secure storage</h3>
        <p>Your photos are stored securely on our servers, so you don't have to worry about losing them. Access your photos from any device with an internet connection.</p>
      </div>
    </div>
  </div>
  <!-- End Features Tes -->
<?php 
    include 'footer.php';
?>