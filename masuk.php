<?php
    include 'header.php';
?>
<div class="container">
    <div class="row">
        <div class="col-4">
        <form class="form-inline" action="/masuk" method="post">
        <div class="form-group mb-2">
            <label for="email" class="sr-only">Email</label>
            <input type="email" class="form-control" id="email" placeholder="Email">
        </div>
        <div class="form-group mb-2">
            <label for="password" class="sr-only">Password</label>
            <input type="password" class="form-control" id="password" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Masuk</button>
        <a ref="daftar"><button type="submit" class="btn btn-secondary mb-2">Daftar</button></div>
        </form>
        </div>
    </div>
</div>
<?php
    include 'footer.php';
?>