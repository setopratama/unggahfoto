<?php
    include 'header.php';
?>
<div class="container">
    <div class="row mt-5">
    <div class="col-lg-2 col-sm-12">
            <img src="https://source.unsplash.com/random/500x500" alt="" class="col-12 col-sm-12">
        </div>
        <div class="col-lg-4 col-sm-12">
            <p style="font-size: 1.5rem;">Aplikasi unggah foto adalah sebuah platform dimana pengguna dapat membagikan foto yang mereka ambil. Aplikasi ini memiliki kategori User-Generated Content (UGC), yang berarti bahwa semua konten yang diunggah ke aplikasi ini dibuat oleh pengguna.</p>
        </div>
    </div>
</div>
<?php 
    include 'footer.php';
?>